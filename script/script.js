var feeds = document.querySelectorAll('.feed');

function feedChoise() {

    if (this.classList.contains('feed_selected')){
        this.classList.remove('feed_selected');
    } else {
        this.classList.add('feed_selected');
    };

    if (this.classList.contains('feed_disabled')){
        this.classList.remove('feed_selected');
    };

};

feeds.forEach(function(item){
    item.addEventListener('click', feedChoise);
});